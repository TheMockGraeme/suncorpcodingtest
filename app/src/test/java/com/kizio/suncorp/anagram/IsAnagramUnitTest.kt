package com.kizio.suncorp.anagram

import com.kizio.suncorp.anagrams.isAnagram
import com.kizio.suncorp.anagrams.isAnagramStrict
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Assert.fail
import org.junit.Test

/**
 * Unit tests for the IsAnagram functions that run on the local machine.
 *
 * @author Graeme Sutherland
 */
class IsAnagramUnitTest {
	/**
	 * Test for a valid anagram.
	 */
	@Test
	fun isAnagram_legalInput() {
		assertTrue(isAnagram("doctorwho", "torchwood"))
	}

	/**
	 * Test for a valid anagram.
	 */
	@Test
	fun isAnagram_illegalInputSpaces() {
		assertTrue(isAnagram("doctor who", "torchwood"))
	}

	/**
	 * Test for a valid anagram.
	 */
	@Test
	fun isAnagram_illegalInputUpperCase() {
		assertTrue(isAnagram("Doctorwho", "Torchwood"))
	}

	/**
	 * Test for an invalid anagram.
	 */
	@Test
	fun isAnagram_noAnagram() {
		assertFalse(isAnagram("doctorwho", "cafe"))
	}

	/**
	 * Test for an invalid anagram.
	 */
	@Test
	fun isAnagram_differentNumberOfLetters() {
		assertFalse(isAnagram("caffe", "cafe"))
	}

	/**
	 * Tests the strict algorithm with an accented character. This would trip an error if a straight
	 * a to z regex was used.
	 */
	@Test
	fun isAnagram_legalInputWithAccent() {
		assertTrue(isAnagram("café", "aféc"))
	}

	/**
	 * Tests the algorithm with an accented character on one word. This would trip an error if a
	 * straight a to z regex was used or the app treats e and é differently.
	 */
	@Test
	fun isAnagram_legalInputWithAccentOnOneWord() {
		assertTrue(isAnagram("café", "face"))
	}

	/**
	 * Tests the algorithm with a German Eszett character.
	 */
	@Test
	fun isAnagram_legalInputWithEszett() {
		assertTrue(isAnagram("oß", "sos"))
	}

	/**
	 * Tests the algorithm with an Icelandic Thorn character.
	 */
	@Test
	fun isAnagram_legalInputWithThorn() {
		assertTrue(isAnagram("oþ", "hot"))
	}

	/**
	 * Tests the strict algorithm with a piece of Greek text.
	 */
	@Test
	fun isAnagram_legalInputWithGreekCharacters() {
		assertTrue(isAnagram("ϰισσύβιον", "σσϰιονύβι"))
	}

	/**
	 * Test for a valid anagram.
	 */
	@Test
	fun isAnagramStrict_legalInput() {
		assertTrue(isAnagramStrict("doctorwho", "torchwood"))
	}

	/**
	 * Test for an invalid anagram input that contains spaces.
	 */
	@Test
	fun isAnagramStrict_illegalInputSpaces() {
		try {
			isAnagramStrict("doctor who", "torchwood")

			fail()
		} catch (iae: IllegalArgumentException) {
			// Exception expected.
		}
	}

	/**
	 * Test for a valid anagram.
	 */
	@Test
	fun isAnagramStrict_illegalInputUpperCase() {
		try {
			isAnagramStrict("Doctorwho", "Torchwood")

			fail()
		} catch (iae: IllegalArgumentException) {
			// Exception expected.
		}
	}

	/**
	 * Test for a valid anagram.
	 */
	@Test
	fun isAnagramStrict_noAnagram() {
		assertFalse(isAnagramStrict("doctorwho", "cafe"))
	}

	/**
	 * Test for a valid anagram.
	 */
	@Test
	fun isAnagramStrict_differentNumberOfLetters() {
		assertFalse(isAnagramStrict("caffe", "cafe"))
	}
	/**
	 * Tests the strict algorithm with an accented character. This would trip an error if a straight
	 * a to z regex was used.
	 */
	@Test
	fun isAnagramStrict_legalInputWithAccent() {
		assertTrue(isAnagramStrict("café", "aféc"))
	}

	/**
	 * Tests the strict algorithm with an accented character on one word. This would trip an error
	 * if a straight a to z regex was used or the app treats e and é differently.
	 */
	@Test
	fun isAnagramStrict_legalInputWithAccentOnOneWord() {
		assertTrue(isAnagramStrict("café", "face"))
	}

	/**
	 * Tests the strict algorithm with a German Eszett character.
	 */
	@Test
	fun isAnagramStrict_legalInputWithEszett() {
		assertTrue(isAnagramStrict("oß", "sos"))
		//assertTrue(isAnagramStrict("oþ", "oth"))
	}

	/**
	 * Tests the strict algorithm with an Icelandic Thorn character.
	 */
	@Test
	fun isAnagramStrict_legalInputWithThorn() {
		assertTrue(isAnagramStrict("oþ", "hot"))
	}

	/**
	 * Tests the strict algorithm with a piece of Greek text.
	 */
	@Test
	fun isAnagramStrict_legalInputWithGreekCharacters() {
		assertTrue(isAnagramStrict("ϰισσύβιον", "σσϰιονύβι"))
	}
}
