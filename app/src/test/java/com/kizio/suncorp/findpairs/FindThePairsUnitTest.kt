package com.kizio.suncorp.findpairs

import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Unit tests for the FindThePairs methods.
 */
class FindThePairsUnitTest {

	/**
	 * Tests the uniquePairs method with the sample values from the problem.
	 */
	@Test
	fun uniquePairs_fromTheExample() {
		assertEquals(3, uniquePairs(3, 1, 4, 5, 7, 8, 9))
	}

	/**
	 * An initial implementation running a minus delta failed because it wasn't picking up when a
	 * value was repeated.
	 */
	@Test
	fun uniquePairs_withNegativeDelta() {
		assertEquals(3, uniquePairs(-2, 22, 18, 20, 24))
	}
	/**
	 * Zero is a real corner case. If we have three identical numbers, let's call them a, b, and c,
	 * then there are three combinations: ab, ac, bc.
	 */
	@Test
	fun uniquePairs_withZeroDelta() {
		assertEquals(3, uniquePairs(0, 1, 1, 1))
	}
}