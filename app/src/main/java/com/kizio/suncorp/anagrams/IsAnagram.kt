package com.kizio.suncorp.anagrams

import java.text.Normalizer

/**
 * Compares a pair of [String] objects to ensure that they contain the same characters.
 *
 * The contract given in the coding test is that both strings are lower case words. This method is a
 * bit more permissive since it ignores spaces, punctuation, or digits, along with case.
 *
 * @param word1 The first word [String] to compare
 * @param word2 The second word [String] to compare
 * @return True if word1 is an anagram of word2, false otherwise
 */
fun isAnagram(word1: String?, word2: String?) : Boolean {
	val result: Boolean

	// Guard condition. Empty or null strings don't contain anagrams.
	result = if (word1 != null && !word1.isBlank() && word2 != null && !word2.isBlank()) {
		val count1 = countCharacters(word1)
		val count2 = countCharacters(word2)

		// The equality operator on the Map class does all the hard work for us.
		count1 == count2
	} else {
		false
	}

	return result
}

/**
 * Compares a pair of [String] objects to ensure that they contain the same characters.
 *
 * This will throw an [IllegalArgumentException] if the input isn't only made up of lowercase
 * characters.
 *
 * @param word1 The first word [String] to compare
 * @param word2 The second word [String] to compare
 * @return True if word1 is an anagram of word2, false otherwise
 */
fun isAnagramStrict(word1: String?, word2: String?) : Boolean {
	val regex = Regex("[\\p{Ll}]+")
	val result: Boolean

	// Guard condition. Null strings don't contain anagrams.
	result = if (word1 == null || word2 == null) {
		false
	} else if (!word1.matches(regex)) {
		throw IllegalArgumentException(word1 + "is not a valid input")
	} else if (!word2.matches(regex)) {
		throw IllegalArgumentException(word2 + "is not a valid input")
	} else {
		val count1 = countCharacters(word1)
		val count2 = countCharacters(word2)

		// The equality operator on the Map class does all the hard work for us.
		count1 == count2
	}

	return result
}

/**
 * This method builds up a [Map] containing the number of instances of each character. This method
 * will skip spaces and punctuation, which is probably what we want.
 *
 * The [String] is run through a [Normalizer] to decompose an accented letter (such as é) into a
 * pair of characters, the character and it's applied mark (such as e and an accent). The call to
 * isLetterOrDigit() will reject the latter, so (for example) café and face will match.
 *
 * The algorithm will replace the German ß with ss, and the Icelandic þ with th. You said you were
 * worried about corner cases...
 *
 * @param string The [String] whose letters we want to count
 * @return A [Map] containing the character counts
 */
private fun countCharacters(string: String) : Map<Char, Int> {
	val lower = string.toLowerCase()
	val replaced = lower.replace("ß", "ss").replace("þ", "th")
	val normalised = Normalizer.normalize(replaced, Normalizer.Form.NFD)
	val count = HashMap<Char, Int>()

	for (c in normalised) {
		// The original implementation used isLetter, which skips digits. I swapped it over because
		// the results feel better with it in place. However, the strict filtering will reject
		// numbers in any case.
		if (c.isLetterOrDigit()) {
			count[c] = count[c]?.plus(1) ?: 1
		}
	}

	return count
}