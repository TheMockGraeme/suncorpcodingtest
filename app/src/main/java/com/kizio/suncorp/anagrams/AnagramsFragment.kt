package com.kizio.suncorp.anagrams

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import com.kizio.suncorp.R
import kotlinx.android.synthetic.main.fragment_anagrams.check_anagram
import kotlinx.android.synthetic.main.fragment_anagrams.first_edit_text
import kotlinx.android.synthetic.main.fragment_anagrams.is_anagram
import kotlinx.android.synthetic.main.fragment_anagrams.is_anagram_stict
import kotlinx.android.synthetic.main.fragment_anagrams.second_edit_text

/**
 * Provides a basic UI for the anagram function.
 *
 * @author Graeme Sutherland
 */
class AnagramsFragment : Fragment() {
	/**
	 * Invoked when the fragment's [View] is to be created.
	 *
	 * @param inflater The [LayoutInflater] used to create the view
	 * @param container The parent [ViewGroup] to which the [View] will be attached
	 * @param savedInstanceState A [Bundle] containing any saved state to preserve
	 * @return The newly created [View]
	 */
	override fun onCreateView(inflater: LayoutInflater,
							  container: ViewGroup?,
							  savedInstanceState: Bundle?): View? {
		return inflater.inflate(R.layout.fragment_anagrams, container, false)
	}

	/**
	 * Invoked when the fragment's [View] has just been created.
	 *
	 * @param view The newly created [View]
	 * @param savedInstanceState A [Bundle] containing any saved state to preserve
	 */
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		check_anagram.setOnClickListener {checkIfAnagram()}
	}

	/**
	 * Checks whether or not the inputs are anagrams.
	 */
	private fun checkIfAnagram() {
		val word1 = first_edit_text.text.toString()
		val word2 = second_edit_text.text.toString()
		val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager

		imm?.hideSoftInputFromWindow(view?.windowToken, 0)

		is_anagram.visibility = View.VISIBLE
		is_anagram_stict.visibility = View.VISIBLE

		is_anagram.text = if (isAnagram(word1, word2)) {
			getString(R.string.is_anagram, word1, word2)
		} else {
			getString(R.string.is_not_anagram, word1, word2)
		}

		is_anagram_stict.text = try {
			if (isAnagramStrict(word1, word2)) {
				getString(R.string.is_anagram_strict, word1, word2)
			} else {
				getString(R.string.is_not_anagram_strict, word1, word2)
			}
		} catch (iae: IllegalArgumentException) {
			getString(R.string.is_illegal_input, word1, word2)
		}
	}
}