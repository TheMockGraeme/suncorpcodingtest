package com.kizio.suncorp.findpairs

/**
 * Implementation of the unique pairs algorithm. This will return the number of integers in the
 * supplied array that are the specified delta apart.
 *
 * Note that the delta and numbers values are flipped from the function specification in the
 * challenge description, as this allows the number list to be passed as a varargs parameter.
 *
 * @param delta An [Int] giving the gap between the numbers
 * @param numbers A variable number of [Int] values used to find pairs
 */
fun uniquePairs(delta: Int, vararg numbers: Int): Int {
	var count = 0

	for (i in 0 until numbers.size - 1) {
		val compareAdd = numbers[i] + delta
		val compareSubtract = numbers[i] - delta

		for (j in (i + 1 until numbers.size)) {
			// Comparing the value both ways means we only need to iterate over each unique pair of
			// values once. This simplifies the code compared with my previous version of the
			// algorithm.
			if (numbers[j] == compareAdd || numbers[j] == compareSubtract) {
				count ++
			}
		}
	}

	return count
}
