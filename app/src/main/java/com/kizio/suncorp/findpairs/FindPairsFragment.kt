package com.kizio.suncorp.findpairs

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import com.kizio.suncorp.R
import kotlinx.android.synthetic.main.fragment_find_pairs.delta
import kotlinx.android.synthetic.main.fragment_find_pairs.find_pairs
import kotlinx.android.synthetic.main.fragment_find_pairs.found_pairs
import kotlinx.android.synthetic.main.fragment_find_pairs.numbers

/**
 * Provides a basic UI for the find pairs function.
 *
 * @author Graeme Sutherland
 */
class FindPairsFragment : Fragment() {
	/**
	 * Invoked when the fragment's [View] is to be created.
	 *
	 * @param inflater The [LayoutInflater] used to create the view
	 * @param container The parent [ViewGroup] to which the [View] will be attached
	 * @param savedInstanceState A [Bundle] containing any saved state to preserve
	 * @return The newly created [View]
	 */
	override fun onCreateView(inflater: LayoutInflater,
							  container: ViewGroup?,
							  savedInstanceState: Bundle?): View? {
		return inflater.inflate(R.layout.fragment_find_pairs, container, false)
	}

	/**
	 * Invoked when the fragment's [View] has just been created.
	 *
	 * @param view The newly created [View]
	 * @param savedInstanceState A [Bundle] containing any saved state to preserve
	 */
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		find_pairs.setOnClickListener {onFindThePairs()}
	}

	/**
	 * Invoked when the find pairs button is clicked.
	 */
	private fun onFindThePairs() {
		val numbersString = numbers.text.toString()
		val deltaString = delta.text.toString()
		val numbersArray = numbersString.split(" ")
		val numbersList = ArrayList<Int>(numbersArray.size)
		val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager

		imm?.hideSoftInputFromWindow(view?.windowToken, 0)

		found_pairs.text = try {
			for (number in numbersArray) {
				val trimmed = number.trim()

				if (!trimmed.isBlank()) {
					numbersList.add(trimmed.toInt())
				}
			}

			val deltaInt = deltaString.toInt()
			val pairs = uniquePairs(deltaInt, *numbersList.toIntArray())

			formatOutput(deltaInt, numbersList, pairs)
		} catch (nfe: NumberFormatException) {
			getString(R.string.find_pairs_error)
		}

		found_pairs.visibility = View.VISIBLE
	}

	/**
	 * Helper method to format the output from the algorithm.
	 *
	 * @param delta The [Int] difference between the pairs
	 * @param numbersList The [List] of numbers entered
	 * @param pairs An [Int] giving the number of pairs found
	 * @return A formatted [String] to display to the user
	 */
	private fun formatOutput(delta: Int, numbersList: List<Int>, pairs: Int) : String {
		val builder = StringBuilder()

		builder.append('[')

		for (i in 0 until numbersList.size) {
			if (i != 0) {
				builder.append(", ")
			}

			builder.append(numbersList[i])
		}

		builder.append(']')

		return getString(R.string.found_pairs, builder.toString(), delta, pairs)
	}
}