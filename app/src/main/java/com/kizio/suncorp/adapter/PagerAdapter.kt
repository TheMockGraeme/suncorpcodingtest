package com.kizio.suncorp.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.kizio.suncorp.anagrams.AnagramsFragment
import com.kizio.suncorp.findpairs.FindPairsFragment

/**
 * Provides the fragments to be displayed in a view page.
 *
 * @author Graeme Sutherland
 * @constructor Creates a new instance of the pager adapter
 * @param fragmentManager The [FragmentManager] used by the activity
 */
class PagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {
	/**
	 * Array holding the [Fragment] objects to display.
	 */
	private val fragments = arrayOf(AnagramsFragment(), FindPairsFragment())

	/**
	 * Returns the [Fragment] associated with a specified position.
	 *
	 * @param position The position of the [Fragment] in the pager
	 * @return The [Fragment] at the specified position
	 */
	override fun getItem(position: Int): Fragment {
		return fragments[position]
	}

	/**
	 * Gets the number of views available.
	 *
	 * @return The number of views available
	 */
	override fun getCount(): Int {
		return fragments.size
	}
}