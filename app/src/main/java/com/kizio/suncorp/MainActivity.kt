package com.kizio.suncorp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.kizio.suncorp.adapter.PagerAdapter
import kotlinx.android.synthetic.main.activity_main.nav_view
import kotlinx.android.synthetic.main.activity_main.view_pager

/**
 * Main activity, used to display a pair of simple fragments to illustrate the methods that have
 * been written for the coding test.
 *
 * @author Graeme Sutherland
 */
class MainActivity : AppCompatActivity() {
    /**
     * The navigation listener used to swap between the fragments being displayed.
     */
    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_anagrams -> {
                view_pager.currentItem = 0
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_findpairs -> {
                view_pager.currentItem = 1
                return@OnNavigationItemSelectedListener true
            }
        }

        false
    }

    /**
     * Invoked when the activity is created. Sets up the content view.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        view_pager.adapter = PagerAdapter(supportFragmentManager)

        nav_view.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
    }
}
